Для запуска приложения требуется Maven. Вы просто заходите в директорию проекта и набираете 
mvn package
mvn tomcat:run -e

Сейчас при запуске проекта база данных каждый раз пересоздается заново. Это удобно для тестирования. 
Изменить это можно в файле hibernate.cfg.xml
<property name="hbm2ddl.auto">create</property> (в комментарии есть пояснения)
Также в файлах hibernate.cfg.xml и spring.properties находится настройка вывода в лог SQL-я 
<property name="show_sql">true</property>
и друге полезные настройки работы с БД

Позже будет создан скрипт первичного разворачивания базы и она будет просто пополняться данными.



http://localhost:8080/jetbilling/jetbillingservice/main/persons


Requests for answers test from POSTMAN:

POST: http://localhost:8080/jetbilling/jetbillingservice/main/persons/add
firstName
lastName
money

GET: http://localhost:8080/jetbilling/jetbillingservice/main/persons/edit?id=2

GET: http://localhost:8080/jetbilling/jetbillingservice/main/persons/add

GET: http://localhost:8080/jetbilling/jetbillingservice/main/persons


