package jetbillingservice.service;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

import jetbillingservice.model.Person;

/** Service for processing Persons */
@Service("personService")


public class PersonService {

	private static Logger logger = Logger.getLogger("personService");

	@Resource(name = "sessionFactory")
	private SessionFactory sessionFactory;

	/** Retrieves all persons 
	 * @return a list of persons */
	@Transactional (readOnly = true)
	public List<Person> getAll() {
		logger.debug("Retrieving all persons");
		Session session = sessionFactory.getCurrentSession();
		// Create a Hibernate query (HQL)
		Query query = session.createQuery("FROM  Person");
		// Retrieve all
		return query.list();
	}

	/** Retrieves a single person */
	@Transactional(readOnly = true)
	public Person get(Integer id) {
		Session session = sessionFactory.getCurrentSession();
		// Retrieve existing person first
		Person person = (Person) session.get(Person.class, id);
		return person;
	}

	/** Adds a new person */
	@Transactional
	public void add(Person person) {
		logger.debug("Adding new person: " + person.toString());
		logger.debug("Adding new person: " + person);
		Session session = sessionFactory.getCurrentSession();
		session.save(person);
	}

	/** Deletes an existing person
	 * @param id - the id of the existing person */
	@Transactional
	public void delete(Integer id) {
		logger.debug("Deleting existing person");
		Session session = sessionFactory.getCurrentSession();
		// Retrieve existing person first
		Person person = (Person) session.get(Person.class, id);
		session.delete(person);
	}

	/** Edits an existing person */
	@Transactional
	public void edit(Person person) {
		logger.debug("Editing existing person");
		Session session = sessionFactory.getCurrentSession();
		// Retrieve existing person via id
		Person existingPerson = (Person) session.get(Person.class, person.getId());
		// Assign updated values to this person
		existingPerson.setFirstName(person.getFirstName());
		existingPerson.setLastName(existingPerson.getLastName());
		existingPerson.setMoney(existingPerson.getMoney());
		// Save updates
		session.save(existingPerson);
	}
}
