package jetbillingservice.service;

import java.util.List;

import javax.annotation.Resource;

import jetbillingservice.model.OperatorAdmin;
import jetbillingservice.model.Person;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("operatorAdminService")
public class OperatorAdminService {
	
	private static Logger logger = Logger.getLogger("operatorAdminService");
	
    @Resource(name="sessionFactory")
    private SessionFactory sessionFactory;
    
	/** Adds a new OA */
	@Transactional
	public void add(OperatorAdmin operatorAdmin) {
		logger.debug("Adding new operator admin: " + operatorAdmin);
		Session session = sessionFactory.getCurrentSession();
		session.save(operatorAdmin);
	}
    
 /* ��������� ���� ������ ����������   [GET] .../OperatorAdmins  */
    public List<OperatorAdmin> getAll() {
        logger.debug("Retrieving all operator-admins");

        Session session = sessionFactory.getCurrentSession();

        // Create a Hibernate query (HQL)
        Query query = session.createQuery("FROM  OperatorAdmin");

        // Retrieve all
        return  query.list();
    }

 /* ��������� ������ ��������� �� ID  [GET] .../OperatorAdmin/id */
    public OperatorAdmin get( Integer id ) {

        Session session = sessionFactory.getCurrentSession();

        // Retrieve existing person first
        OperatorAdmin operatorAdmin = (OperatorAdmin) session.get(OperatorAdmin.class, id);

        return operatorAdmin;
    }
    
 /* ��������� ������ ���� �������� ��������� �� ��� ID  [GET] .../OperatorAdmin/id/Operations */
    
 /* ��������� ������ ���� ���������� �������� ��������� �� ��� ID 
  * (���� �������� ������ ������) [GET] .../OperatorAdmin/id/Operation?isActual=true */
   
 /* ��������� ������ ���� ���������� ��������� �� ��� ID  [GET] .../OperatorAdmin/id/Transactions */   

 /* [GET] .../OperatorAdmin/id/CurrentStatus */
    
 /* [POST] .../OperatorAdmin { name: ��, email:��,  } */
    
 /* [POST] .../OperatorAdmin/id/Operation */

}
