package jetbillingservice.model;

import javax.persistence.*;

import java.io.Serializable;

import jetbillingservice.service.DefaultFieldsExtension;


@Entity
@Table(name = "PERSON")
public class Person extends DefaultFieldsExtension implements Serializable {

	private static final long serialVersionUID = -5527566248002296042L;

	@Column(name = "FIRST_NAME")
	private String firstName;

	@Column(name = "LAST_NAME")
	private String lastName;

	@Column(name = "MONEY")
	private Double money;


	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Double getMoney() {
		return money;
	}

	 public void setMoney(Double money) {
		this.money = money;
	}

}
