package jetbillingservice.controller;

import java.util.List;

import javax.annotation.Resource;

import jetbillingservice.model.Person;
import jetbillingservice.service.PersonService;


import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/main")
public class MainController {

	private static Logger logger = Logger.getLogger("mainController");

	@Resource(name = "personService")
	private PersonService personService;

	
	@RequestMapping(value = "/persons", method = RequestMethod.GET, 
			produces = "application/json", consumes = "application/json")
	public @ResponseBody
	  List<Person> getPersons() {

		logger.debug("Received request to show all persons");
		List<Person> persons = personService.getAll();

		return persons;
	}

	
	/** Adds a new person by delegating the processing to PersonService.
	 * @return added person in JSON */
	@RequestMapping(value = "/persons/add", method = RequestMethod.POST, 
			produces = "application/json", consumes = "application/json")
	public Person add(@RequestBody Person person) {
		logger.debug("Received request to add new person: " + person);

		personService.add(person);
		return person;
	}
  
	/** Deletes an existing person by delegating the processing to PersonService.
	 * @return empty JSON */
	@RequestMapping(value = "/persons/delete", method = RequestMethod.GET,
			produces = "application/json", consumes = "application/json")
	public @ResponseBody
	String delete(@RequestParam(value = "id", required = true) Integer id) {

		logger.debug("Received existing person deleting request");

		personService.delete(id);

		// empty JSON emulate
		return "{}";
	}

	/** Edits an existing person by delegating the processing to PersonService.
	 * @return edited person in JSON */
	@RequestMapping(value = "/persons/edit", method = RequestMethod.POST,
			produces = "application/json", consumes = "application/json")
	public @ResponseBody
	Person saveEdit(@ModelAttribute("person") Person person,
	        @RequestParam(value = "id", required = true) Integer id) {
		logger.debug("Received request to update person");

		person.setId(id);

		personService.edit(person);

		return person;
	}

}