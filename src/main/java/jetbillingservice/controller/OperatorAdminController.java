package jetbillingservice.controller;

import javax.annotation.Resource;

import jetbillingservice.model.OperatorAdmin;
import jetbillingservice.service.OperatorAdminService;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/opadm")
public class OperatorAdminController {

	private static Logger logger = Logger.getLogger("operatorAdminController");
	
	@Resource(name = "operatorAdminService")
	private OperatorAdminService operatorAdminService;
	
	
	/** Adds a new OA by delegating the processing to OperatorAdminService.
	 * @return added OA in JSON */
	@RequestMapping(value = "/opadms/add", method = RequestMethod.POST, 
			produces = "application/json", consumes = "application/json")
	public OperatorAdmin add(@RequestBody OperatorAdmin operatorAdmin) {
		logger.debug("Received request to add new operator admin: " + operatorAdmin);

		operatorAdminService.add(operatorAdmin);
		return operatorAdmin;
	}
	
	/* ��������� ������ ��������� �� ID [GET] .../OperatorAdmin/id */
	
 	/* ��������� ������ ���� �������� ��������� �� ��� ID [GET]
	 * .../OperatorAdmin/id/Operations */

	/* ��������� ������ ���� ���������� �������� ��������� �� ��� ID (����
	 * �������� ������ ������) [GET]
	 * .../OperatorAdmin/id/Operation?isActual=true */

	/*
	 * ��������� ������ ���� ���������� ��������� �� ��� ID [GET]
	 * .../OperatorAdmin/id/Transactions
	 */

	/* [GET] .../OperatorAdmin/id/CurrentStatus */

	/* [POST] .../OperatorAdmin { name: ��, email:��, } */

	/* [POST] .../OperatorAdmin/id/Operation */

}
